package com.os.dao;


import java.sql.ResultSet;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.os.bean.Cart;
import com.os.bean.Datapoints;
import com.os.bean.Products;
import com.os.bean.Report;



public class CartDao {
	JdbcTemplate template;

	public JdbcTemplate getTemplate() {
		return template;
	}

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}  
	public int save(String product,String email){String sql;  Products e=new Products();
		template.query("select * from Products where product='"+product+"'",new RowMapper<Products>(){  
		
		public Products mapRow(ResultSet rs, int row) throws SQLException {  
	            
	            e.setSrno(rs.getInt(1));
	            e.setProduct(rs.getString(2));  
	            e.setProductid(rs.getInt(3));
	            e.setPrice(rs.getInt(4));
	            e.setQuantity(rs.getInt(5));
	            e.setImage(rs.getString(6));
				return e;
	            
	        }  
	    });
		
		 sql="insert into Cart(product,productid,quantity,price,image,email) values('"+e.getProduct()+"',"+e.getProductid()+",'"+e.getQuantity()+"','"+e.getPrice()+"','"+e.getImage()+"','"+email+"')";  
 	    return template.update(sql);   
	}  
	public List<Cart> getProducts(String email){  
		return template.query("select * from Cart where email='"+email+"'",new RowMapper<Cart>(){  
	        public Cart mapRow(ResultSet rs, int row) throws SQLException {  
	            Cart e=new Cart();  
	        
	            e.setProduct(rs.getString(1));  
	            e.setProductid(rs.getInt(2));
	            e.setPrice(rs.getInt(3));
	            e.setQuantity(rs.getInt(4));
	            e.setImage(rs.getString(5));
	           return e;  
	        }  
	    });  
	} 
	public int delete(int id){  
	    String sql="delete from Cart where productid="+id+"";  
	    return template.update(sql);  
	} 
public int report(int id,int amount,String email) {
	String status="Processing";
	String sql="insert into report(orderid,amount,status,email) values('"+id+"',"+amount+",'"+status+"','"+email+"')";  
	    return template.update(sql);   
}
public List<Report> getReport(String email){  
    return template.query("select * from Report where email='"+email+"'",new RowMapper<Report>(){  
        public Report mapRow(ResultSet rs, int row) throws SQLException {  
           Report r=new Report();
           r.setOrderid(rs.getInt(1));
           r.setOrderdate(rs.getDate(2));
           r.setAmount(rs.getInt(3));
           r.setStatus(rs.getString(4));
           return r;
        
            
        }  
    });  
} List<List<Datapoints>> list = new ArrayList<List<Datapoints>>();
static List<Datapoints> da= new ArrayList<Datapoints>();
                          public List<List<Datapoints>>  getDataPoints(){
                        	  String sql = "select * from datapoints";
                              
                              try {
                              	da = template.query(sql, new RowMapper<Datapoints>() {
                       
                      				@Override
                      				public Datapoints mapRow(ResultSet rs, int rowNum) throws SQLException {
                      	            	Datapoints dataPoint = new Datapoints();
                      	     
                      	            	dataPoint.setX(rs.getInt("x"));
                      	            	dataPoint.setY(rs.getInt("y"));
                      	     
                      	                return dataPoint;
                      				}});
                              }
                              catch(Exception e){
                              	da = null;
                              	
                              }
                      		list.add(da);
							return list;
                      	}

}
