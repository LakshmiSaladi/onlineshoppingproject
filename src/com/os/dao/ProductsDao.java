package com.os.dao;
import java.sql.ResultSet;  


import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;
import com.os.bean.Products;
import com.os.bean.Report;;

public class ProductsDao {
	JdbcTemplate template;  
	  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
	public int save(Products p){  
	    String sql="insert into products(srno,product,productid,price,quantity,image) values('"+p.getSrno()+"','"+p.getProduct()+"',"+p.getProductid()+",'"+p.getPrice()+"','"+p.getQuantity()+"','"+p.getImage()+"')";  
	    return template.update(sql);  
	}  
	public int update(Products p){  
	    String sql="update Products set price='"+p.getPrice()+"', product='"+p.getProduct()+"',quantity='"+p.getQuantity()+"',image='"+p.getImage()+"' where productid="+p.getProductid()+"";  
	    return template.update(sql);  
	}  
	public int delete(int id){  
	    String sql="delete from Products where productid="+id+"";  
	    return template.update(sql);  
	}  
	public Products getProductById(int id){  
	    String sql="select * from Products where productid=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Products>(Products.class));  
	}  
	public List<Products> getProducts(){  
	    return template.query("select * from Products ORDER BY srno",new RowMapper<Products>(){  
	        public Products mapRow(ResultSet rs, int row) throws SQLException {  
	            Products e=new Products();  
	            e.setSrno(rs.getInt(1));  
	            e.setProduct(rs.getString(2));  
	            e.setProductid(rs.getInt(3));
	            e.setPrice(rs.getInt(4));
	            e.setQuantity(rs.getInt(5));
	            e.setImage(rs.getString(6));
	            return e;  
	        }  
	    });  
	} 
	public List<Products> getAllProducts(){
		return template.query("select product,image from Products",new RowMapper<Products>(){  
	        public Products mapRow(ResultSet rs, int row) throws SQLException {  
	            Products e=new Products();  
	         	            e.setProduct(rs.getString(1));
	         	            e.setImage(rs.getString(2));
	            return e;  
	        }  
	    });  
	}
	public List<Products> getDetails(String product){
		return template.query("select * from Products where product='" +product+ "'",new RowMapper<Products>(){  
	        public Products mapRow(ResultSet rs, int row) throws SQLException {  
	            Products e=new Products();  
	            e.setSrno(rs.getInt(1));  
	            e.setProduct(rs.getString(2));  
	            e.setProductid(rs.getInt(3));
	            e.setPrice(rs.getInt(4));
	            e.setQuantity(rs.getInt(5));
	            e.setImage(rs.getString(6));
	            return e;  
	        }  
	    });  
	}  
	public List<Report> report(){  
	    return template.query("select * from Report",new RowMapper<Report>(){  
	        public Report mapRow(ResultSet rs, int row) throws SQLException {  
	            Report e=new Report();  
	            e.setOrderid(rs.getInt(1));
	            e.setOrderdate(rs.getDate(2));
	            e.setAmount(rs.getInt(3));
	            e.setStatus(rs.getString(4));
	            return e;  
	        }  
	    });  
	} 
} 
