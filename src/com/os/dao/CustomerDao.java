package com.os.dao;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.os.bean.Customer;
import com.os.bean.Products;

public class CustomerDao {
	JdbcTemplate template;  
	  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
	public int save(Customer r){  
	    String sql="insert into Customer(name,email,password,phno) values('"+r.getName()+"','"+r.getEmail()+"',"+r.getPassword()+",'"+r.getPhno()+"')";  
	    return template.update(sql);  
	}  
	
	public int getlogin(String email,String password){  
	    String sql="select * from Customer where email='"+email+"' and  password='"+password+"'";  
	    return template.update(sql);  
	}  
	public List<Products> getcart(String productid){
		return template.query("select * from Products where productid='"+productid+ "'",new RowMapper<Products>(){  
	        public Products mapRow(ResultSet rs, int row) throws SQLException {  
	            Products e=new Products();  
	            e.setSrno(rs.getInt(1));  
	            e.setProduct(rs.getString(2));  
	            e.setProductid(rs.getInt(3));
	            e.setPrice(rs.getInt(4));
	            e.setQuantity(rs.getInt(5));
	            return e;  
	        }  
	    });  
	}  

}
