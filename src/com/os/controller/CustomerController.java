package com.os.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.os.bean.Customer;
import com.os.dao.CustomerDao;

@Controller
public class CustomerController {
@Autowired
CustomerDao cdao;

@RequestMapping("/account")
public String account() {
	return "register";
}


@RequestMapping("/customerregister")
public String register(@ModelAttribute("r") Customer r) {
	  cdao.save(r);
	return "Success";  
}


@RequestMapping("/customerlogin")
public String loged() {
	return "customerlogin";
}



@RequestMapping(value = "dologin", method = RequestMethod.GET)
public String login(@RequestParam("email") String email,@RequestParam("password") String password,@ModelAttribute ("Customer") Customer cust, HttpServletRequest request, HttpSession session,ModelMap modelMap) throws SQLException {
	int k=cdao.getlogin(email, password);
	if(k>0) {
		 session.setAttribute("name",cust.getEmail() );
		return "Success";
	} else {
		modelMap.put("error", "Invalid Account");
		return "customerlogin";	
	}
	
}

@RequestMapping(value = "logout", method = RequestMethod.GET)
public String logout(HttpSession session) {
	session.removeAttribute("name");
	return "customerlogin";
}



}
