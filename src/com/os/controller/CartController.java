package com.os.controller;

import java.io.IOException;


import java.text.ParseException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.os.bean.Cart;
import com.os.bean.Datapoints;
import com.os.bean.Report;

import com.os.dao.CartDao;

@Controller
@RequestMapping(value = "cart")
public class CartController {
@Autowired 
CartDao cartdao;
	@RequestMapping(value = "buy/{product}", method = RequestMethod.GET)
	public String addtocart(@PathVariable String product,Model m,HttpSession session, HttpServletRequest request) {
		String email=(String)request.getSession().getAttribute("name");	
		if(email!=null) {
		cartdao.save(product,email);
	    	  List<Cart> list=cartdao.getProducts(email);
	          m.addAttribute("list",list);
	          return "cartpage";
		
	      }else {
	    	  return "middle";
	      }
	}
	@RequestMapping(value = "/continue", method = RequestMethod.GET)
	public String continueshop() {
		return "redirect:/allproducts";
	}
	@RequestMapping(value = "/checkout/{total}", method = RequestMethod.GET)
	public String checkout(@PathVariable("total") int total,Model m,HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		String n=""+total;
		request.setAttribute("n", n);
		
		return "checkout";
	}
	@RequestMapping(value = "/remove/{productid}", method = RequestMethod.GET)
	public String remove(@PathVariable int productid,String product,Model m,HttpSession session, HttpServletRequest request) {
		String email=(String)request.getSession().getAttribute("name");	
        cartdao.delete(productid);
        List<Cart> list=cartdao.getProducts(email);
        m.addAttribute("list",list);
        return "cartpage"; 
       
    }  
	 @RequestMapping("/order")
     public String order(Model m,HttpSession session, HttpServletRequest request) {
 		String email=(String)request.getSession().getAttribute("name");	
   	  List<Cart> list=cartdao.getProducts(email);
     m.addAttribute("list",list);
		return "confirm";  
     }
	 @RequestMapping(value = "/report/{total}/{data}", method = RequestMethod.GET)
		public String report(@PathVariable("total") int total,@PathVariable("data") int data,Model m,HttpServletRequest request,HttpSession session) throws ParseException {
		int amount=total;
		int id=data;
		String email=(String)request.getSession().getAttribute("name");	
	 cartdao.report(id, amount,email);
	 List<Report> list=cartdao.getReport(email);
     m.addAttribute("list",list);
		return "report";  
     }
	 @RequestMapping(value="/canvasjschart",method = RequestMethod.GET)
		public String springMVC(ModelMap modelMap) {
			List<List<Datapoints>> list=cartdao.getDataPoints();
			modelMap.addAttribute("list",list);
			return "chart";
		}
	 
	}
	
