package com.os.controller;

import java.util.List;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.os.bean.Products;
import com.os.bean.Report;
import com.os.dao.ProductsDao;
@Controller
public class ProductController {
	 @Autowired  
	    ProductsDao dao;

     @RequestMapping("/adminlogin")
     public String admin() {
   	 
		return "adminlogin";  
     }
     @RequestMapping("/addproducts")
     public String page() {
		return "addproducts";  
     }
   
	 @RequestMapping("/login")
     public String login(HttpServletRequest req,Model m) {
   	   String name=req.getParameter("name");
   	   String pass=req.getParameter("password");
		 if(name.equals("lakshmi") && pass.equals("567")) {
			 return "addproducts";
		 }else {
		return "adminlogin";  
     }
		 
	 }
	      @RequestMapping("/add")
	      public String addProducts(@ModelAttribute("p") Products p) {
	    	  dao.save(p);
			return "redirect:viewproducts";  
	      }
	      @RequestMapping("/viewproducts")  
	      public String viewProducts(Model m){  
	          List<Products> list=dao.getProducts();  
	          m.addAttribute("list",list);
	          return "viewproducts";  
	      } 
	      @RequestMapping("/allproducts")
	      public String allP(Model m) {
	    	  List<Products> list=dao.getAllProducts();  
	          m.addAttribute("list",list);
	          return "allproducts";  
	    	 
	    	  
	      }
	      
	      @RequestMapping(value="/editproducts/{productid}")
	      public String editProducts(@PathVariable int productid, Model m) {
	    	  Products p=dao.getProductById(productid);  
	          m.addAttribute("command",p);
	          return "editproducts";  
	      }
	      @RequestMapping(value="/edit",method = RequestMethod.POST)
	      public String update(@ModelAttribute("p") Products p) {
	    	  dao.update(p);
			return "redirect:viewproducts";   
	      }
	      @RequestMapping(value="/deleteproducts/{productid}",method = RequestMethod.GET)  
	      public String delete(@PathVariable int productid){  
	          dao.delete(productid);  
	          return "redirect:/viewproducts";
	      }  
	      @RequestMapping(value="/details/{product}",method = RequestMethod.GET)
	      public String details(@PathVariable String product,Model m) {
	    	  List<Products> list=dao.getDetails(product);
	          m.addAttribute("list",list);
	          return "details"; 
	      }
	      @RequestMapping(value="/adminreport")
	      public String adminreport(Model m) {
			List<Report> list=dao.report();
			 m.addAttribute("list",list);
	    	  return "adminreport";
	    	    
	      }
	      
}
