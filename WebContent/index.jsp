<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color:#FFEFD5;
}

li {
  float: left;
}

li a {
  display: block;
  color: #FFEFD5;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
   color:#B22222;
}

li a:hover {
  background-color:green;
}
.myDiv {
  background-color:#B6B6B6;
  text-align:left;
  color:#FFEFD5;
}
.small {
  line-height: 0.1;
  font-family: Times new roman;
  
}
.align {
  width: 1000px;
  height: 1000px;
  position: absolute;
  left: 10%;
  
}
.navbar {
  overflow: hidden;
  background-color:#FFEFD5;
}

.navbar a {
  float:left;
  font-size: 16px;
  color: #B22222;
  text-align: left;
  padding: 14px 16px;
  text-decoration: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: #B22222 ;
  padding: 14px 16px;
  background-color: #FFEFD5;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color:green;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}
.dropdown-content a:hover {
  background-color:#FFEFD5;
}

.dropdown:hover .dropdown-content {
  display:block
}
</style>
</head>
<body class="myDiv">

<div class="navbar">
  <a href="home.jsp" target="home">Home</a>
  <a href="adminlogin" target="home">AdminLogin</a>
  <div class="dropdown">
    <button class="dropbtn">Customer
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="customerlogin" target="home">Login</a>
      <a href="account" target="home">Create Account</a>
      <a href="logout" target="home">LogOut</a>
    </div>
    
  </div> 
   <a href="cart/canvasjschart" target="home">Report</a>
</div>

<br><div class="align">
<h1 class="small">ONLINE SHOPPING SYSTEM</h1>
<p class="small">Free delivery, Great discounts,variety of products</p>
<iframe src="top.jsp"  width="100%" height="30"  style="background-color:white;">
</iframe>
<iframe  src="home.jsp" name="home" width="100%" height="100%"  style="background-color:white;">
</iframe>
</div>
</body>
</html>