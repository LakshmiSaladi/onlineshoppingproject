<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html>
<html>
<head>
<style>
a:link, a:visited {
  background-color:#213213;
  color: white;
  padding: 14px 25px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a:hover, a:active {
  background-color: green;
}
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Product Details</h1>
<hr>
	<table>
	 <c:forEach var="p" items="${list}"> 
    <tr>
    
    <td>ProductName:</td><td>${p.product}</td></tr>
    <tr><td>ProductId:</td><td>${p.productid}</td></tr>
    <tr><td>Price:</td><td>${p.price}</td></tr>
<tr><td>Quantity:</td><td>${p.quantity}</td></tr>
<tr><td>Image:</td><td><img src="${p.image}" width="300" height="300"/></td>
</tr>
   <tr> <td><a href="${pageContext.request.contextPath }/cart/buy/${p.product}">Add To Cart</a></td>
    <td><a href="${pageContext.request.contextPath }/cart/continue">Continue Shopping</a></td></tr>

    </c:forEach>
    </table>
   
</body>
</html>