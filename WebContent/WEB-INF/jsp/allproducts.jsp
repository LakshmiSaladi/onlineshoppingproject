<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
    
<!DOCTYPE html>
<html>
<head>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 8px;
  text-align: left;
  border-bottom: 1px solid #ddd;
}

tr:hover {background-color:#f5f5f5;}
</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Available Products</h1>
	<table>
	<tr><th>PRODUCTS</th></tr>
    <c:forEach var="p" items="${list}"> 
    <tr><td><img src="${p.image}" width="300" height="300"/></td>
    <td><a href="details/${p.product}">${p.product}</a></td>
 </tr>   </c:forEach>
    </table>
    <br/>
   </body>
</html>