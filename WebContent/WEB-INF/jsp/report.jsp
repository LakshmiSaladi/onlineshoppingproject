<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html>
<html>
<head>
<style>

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
</style>

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Order report</h2><table>
<tr><th>OrderId</th><th>OrderDate</th><th>Amount</th><th>Status</th><th>Details</th></tr>
    <c:forEach var="p" items="${list}"> 
    <tr>
    <td>${p.orderid}</td>
    <td>${p.orderdate}</td>
    <td>${p.amount}</td>
    <td>${p.status}</td>
    <td><a href="${pageContext.request.contextPath }/cart/order">View Details</a></td>
    </tr>
    </c:forEach>
    </table>
 <h1>Thank You.... Shop Again</h1>
</body>
</html>