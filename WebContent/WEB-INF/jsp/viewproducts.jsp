<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Available Products</h1>
	<table border="2" width="70%" cellpadding="2">
	<tr><th>SR.NO</th><th>PRODUCT</th><th>PRODUCTID</th><th>PRICE</th><th>QUANTITY</th><th>IMAGE</th><th>EDIT</th><th>REMOVE</th></tr>
    <c:forEach var="p" items="${list}"> 
    <tr>
    <td>${p.srno}</td>
    <td>${p.product}</td>
    <td>${p.productid}</td>
    <td>${p.price}</td>
<td>${p.quantity}</td>
<td><img src="${p.image}" width="50" height="50"/></td>
    <td><a href="editproducts/${p.productid}">Edit</a></td>
    <td><a href="deleteproducts/${p.productid}">Remove</a></td>
</tr>
    </c:forEach>
    </table>
    <br/>
    <a href="addproducts">Add New Product</a>
    
    <a href="adminreport">Report</a>
</body>
</html>