<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Cart Page</h1>
     
    
<c:set var="total" scope="application" value="${0}" />
	<table border="2" width="70%" cellpadding="2"> <tr><th>PRODUCT</th><th>PRODUCTID</th><th>QUANTITY</th><th>PRICE</th><th>IMAGE</th><th>REMOVE</th></tr>
   
	
	 <c:forEach var="p" items="${list}"> 
	 <c:set var="total" value="${total +p.quantity}" />
    <tr>
   
    <td>${p.product}</td>
    <td>${p.productid}</td>
  

  <td>${p.price}</td><td>${p.quantity}</td>
<td><img src="${p.image}" width="300" height="300"/></td>
        <td><a href="${pageContext.request.contextPath }/cart/remove/${p.productid}">Remove</a></td>
</tr>
    </c:forEach>
<tr><td>Total Amount......</td><td></td><td></td><td>${total}</td></tr>
    </table>
    <h3><a href="${pageContext.request.contextPath }/cart/continue">Continue Shopping</a></h3>
    <h3><a href="${pageContext.request.contextPath }/cart/checkout/${total}">CheckOut</a></h3>
</body>
</html>