<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  <%@page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%!
Random randomValue=new Random();
%>
<%! int data=(Math.abs(randomValue.nextInt())%500)+100; %>  
<h2>Your Order Details Are Here:</h2>                 
       <p>Order Id:<%=data%></p>

<c:set var="data" value="<%=data%>" />
<c:set var="total" value="${0}" /><c:set var="subtotal" value="${0}" />
<table border="2" width="70%" cellpadding="2"> 
<tr><th>PRODUCT</th><th>PRODUCTID</th><th>QUANTITY</th><th>Cost Per PRICE</th><th>IMAGE</th><th>Total</th></tr>
    <c:forEach var="p" items="${list}"> 
	 <c:set var="subtotal" value="${p.quantity*p.price}" />
    
	 <c:set var="total" value="${total +(p.quantity*p.price)}" />
    <tr>
   
    <td>${p.product}</td>
    <td>${p.productid}</td>
    
<td>${p.quantity}</td>
<td>${p.price}</td>
<td><img src="${p.image}" width="300" height="300"/></td>
        <td>${subtotal}</td>
</tr>
    </c:forEach>
<tr><td>Total Amount......</td><td></td><td></td><td></td><td></td><td>${total}</td></tr>

    </table>
  <a href="${pageContext.request.contextPath }/cart/report/${total}/${data}" target="home">Report</a>
  
</body>
</html>