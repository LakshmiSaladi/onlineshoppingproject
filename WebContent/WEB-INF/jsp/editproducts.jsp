<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

		<h1>Edit Products</h1>
       <form:form method="POST" action="/onlineshopping1/edit">  
      	<table >  
      	 <tr>  
          <td>ProductId:</td>  
          <td><form:hidden path="productid" /></td>
         </tr> 
      	<tr>
      	<td>SR.No</td>  
         <td><form:input path="srno" /></td>
         </tr> 
         <tr>  
          <td>Product: </td> 
          <td><form:input path="product"  /></td>
         </tr>  
        
         <tr>  
          <td>Price:</td>  
          <td><form:input path="price" /></td>
         </tr> 
         <tr>  
          <td>Quantity:</td>  
          <td>
          <select  name="Quantity">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></td>
         </tr> 
          <tr>  
          <td>Image:</td>  
          <td><form:input path="image" /></td>
         </tr> 
         
         <tr>  
          <td> </td>  
          <td><input type="submit" value="EditProduct" /></td>  
         </tr>  
        </table>  
       </form:form>  

</body>
</html>