<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h3>Make Payment</h3>
<form action="${pageContext.request.contextPath }/cart/order">

 <div class="container">
    <h4>Please fill in this form to Make Payment and Provide Shipping Address </h4>
    <h4>Cash On Delivery</h4>
    <hr>
 
    Total Amount:<%= request.getAttribute("n") %>
    
    <br><br>
    
	<h3>Shipping Address</h3><br><br>
    <label for="hn"><b>House Number:</b></label>
    <input type="text" placeholder="Enter House No." name="hn" id="hn" required>
<br><br>
<label for="st"><b>House Number:</b></label>
    <input type="text" placeholder="Enter Street" name="st" id="st" required>
    <br><br>
    <label for="ci"><b>City:</b></label>
    <input type="text" placeholder="Enter City" name="ci" id="ci" required>
   <br><br> <label for="sta"><b>State:</b></label>
    <input type="text" placeholder="Enter State" name="sta" id="sta" required>
 
   <br><br>
    <button type="submit" class="registerbtn">Confirm Order</button>
</div>

</form>
</body>
</html>