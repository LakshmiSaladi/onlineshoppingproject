<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Top</title>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;

  background-color:#FFEFD5;
}

li {
  float: left;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 0px 10px;
  text-decoration: none;
   color:#B22222;
}

li a:hover {
  background-color:green;
}
</style>
</head>
<body>

<ul>
 <li><a href="allproducts" target="home">All Products</a></li>
 <li><a href="" target="home">Dresses</a></li>
 <li><a href="" target="home">Jewellery</a></li>
 <li><a href="" target="home">FootWear</a></li>
 <li><a href="" target="home">Toys</a></li>
 <li><a href="" target="home">Trending Products</a></li>
<li><a href="contact.html" target="home">Contact Us</a></li>
</ul>

</body>
</html>